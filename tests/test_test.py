import os

from bitbucket_pipes_toolkit.test import PipeTestCase


class PipeTestCaseTestCase(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        with open(os.path.join(os.getcwd(), 'Dockerfile'), 'w') as f:
            f.write("FROM alpine")
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        os.remove(os.path.join(os.getcwd(), 'Dockerfile'))
        super().tearDownClass()

    def test_the_test(self):
        result = self.run_container('echo hello world')
        self.assertIn('hello world', result)
